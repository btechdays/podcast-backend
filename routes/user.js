const express = require('express');
const connection = require('../connection');
const router = express.Router();
const path = require('path');
const util = require('util');
const { url } = require('inspector');

const app = express();


// PAYLOAD    {"uname":"saurav","contact":"1234567890","email":"srv@gmail.com","username":"srv","pass":"admin","songdesc":"Romantic"}
router.post('/signUp', (req, res, next) => {
    let userData = req.body;
    // if (student.enrollmentNo != null, student.sname != null, student.email != null, student.username != null, student.pass != null) {
    var queryy = "select * from users where username=?";
    connection.query(queryy, [userData.username], (err, results) => {
        if (!err) {
            if (results.length <= 0) {
                query = "insert into users (uname,contact,email,username,pass,songdesc) values(?,?,?,?,?,?)";
                connection.query(query, [userData.uname, userData.contact, userData.email, userData.username, userData.pass, userData.songdesc], (err, results) => {
                    if (!err) {
                        return res.status(200).json({ message: "Registered Successfully" });
                    }
                    else
                        return res.status(500).json(err);
                });
            }
            else {
                return res.status(200).json({ message: "Username Already Exists. \n try with another Username" });
            }
        } else
            return res.status(500).json(err);
        // }
        // return res.status(400).json("Insufficient Data");
    })
});

//PAYLOAD     {"username":"srv","pass":"admin"}
router.post('/login', (req, res, next) => {
    //const enroll = req.params.enroll;
    let userData = req.body;
    let userRoles = new Map();
    var query = "select u.id,u.uname,u.contact,u.email,u.username,u.songdesc from users u where username=? and pass=?";
    connection.query(query, [userData.username, userData.pass], (err, results) => {
        if (!err) {
            if (results.length <= 0) {
                return res.status(401).json({ message: "Invalid Username or Password" });
            }
            console.log(results);
            // userRoles.set("role",results.)
            return res.status(200).json({ message: results });
        }
        else {
            return res.status(500).json(err);
        }
    })
});

// PAYLOAD   pathvariable = "subscribed/unsubscribed"    body = {"userid":"1"}
router.post('/getUsers/:status', (req, res, next) => {
    const status = req.params.status;
    console.log(status);

    let userData = req.body;

    let course = req.body;
    if (status == "subscribed") {
        var query = "select u.id,u.username,u.songdesc from users u where u.id in (select s.subscribed_fk from subscribe s where s.user_fk=?)";
        connection.query(query, [userData.userid], (err, results) => {
            if (!err) {
                return res.status(200).json(results);
            }
            else {
                return res.status(500).json(err);
            }
        });
    }
    else if (status == "unsubscribed") {
        var query = "select u.id,u.username,u.songdesc from users u where u.id !=? and u.id not in (select s.subscribed_fk from subscribe s where s.user_fk=?)";
        connection.query(query, [userData.userid, userData.userid], (err, results) => {
            if (!err) {
                return res.status(200).json(results);
            }
            else {
                return res.status(500).json(err);
            }
        });
    }
    else {
        return res.status(400).json("Bad Request.");
    }
});

// PAYLOAD   pathvariable = "subscribed/unsubscribed"   {"usersfk":"1","subscribedfk":"2"}
router.post('/subscribe/:status', (req, res, next) => {
    const status = req.params.status;
    let userData = req.body;

    if (status == "subscribed") {
        query = "insert into subscribe (user_fk,subscribed_fk) values(?,?)";
        connection.query(query, [userData.usersfk, userData.subscribedfk], (err, results) => {
            if (!err) {
                return res.status(200).json({ message: "Subscribed" });
            }
            else
                return res.status(500).json(err);
        });
    }
    else if (status == "unsubscribed") {
        query = "delete from subscribe where user_fk=? and subscribed_fk=?";
        connection.query(query, [userData.usersfk, userData.subscribedfk], (err, results) => {
            if (!err) {
                return res.status(200).json({ message: "UnSubscribed" });
            }
            else
                return res.status(500).json(err);
        });
    }
    else {
        return res.status(400).json("Bad Request.");
    }
});


module.exports = router;