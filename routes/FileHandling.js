const express = require('express');
const connection = require('../connection');
const router = express.Router();
const fileupload = require("express-fileupload");
const path = require('path');
const util = require('util');
const ms = require('mediaserver');

const { url } = require('inspector');
const fs = require('fs');
const app = express();


// router.post('/audioUpload',(req,res,next)=>{
//     if(req.files){
//     console.log(req.files)
//     var fs = require('fs');
//     //fs.mkdirSync('E:/BTech Days Projects/storedFiles/testNodeJss');

//     var file = req.files.file
//     var filename = file.name
//     console.log(filename)

//     file.mv('E:/BTech Days Projects/storedFiles/testNodeJss'+filename,function(err){
//         if(err){
//            res.status(200).json(err);
//         }else{
//             res.status(200).json("File Uploaded");
//         }
//     })
//     return res.status(200).json("hello it worked");
//     }else{
//         // file.mv('E:/BTech Days Projects/storedFiles/testNodeJss'+'abc.mp3',function(err){
//         //     if(err){
//         //         res.send(err)
//         //     }else{
//         //         res.send("File uploaded")
//         //     }
//         // })
//         return res.status(500).json("Oops not working");
//     }
// });

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(fileupload);

app.use(express)

app.listen(8000, () => {
    // console.log("Server listening on port 8000");
})


const BaseURL = "E:/BTech Days Project/storedFiles/";


router.post("/upload/:userid/:username", async (req, res) => {
    try {
        const username = req.params.username;
        const userid = req.params.userid;

        console.log(username);
        const file = req.files.file;
        const fileName = file.name;
        const size = file.data.length;
        const extension = path.extname(fileName);

        console.log(fileName + "-----" + size + "-------" + extension)

        if (!fs.existsSync(BaseURL + username)) {
            fs.mkdirSync(BaseURL + username);
        }

        console.log(BaseURL + username + "/" + fileName);

        if (!fs.existsSync(BaseURL + username + "/" + fileName)) {
            const allowedExtension = /mp3/;
            if (!allowedExtension.test(extension)) throw "Unsupported extension";
            if (size > 5000000000000) throw "File must be less than 5Mb"
            const md5 = file.md5;
            const URL = BaseURL + username + "/" + fileName;

            await util.promisify(file.mv)(URL);

            var query = "insert into userSongs (user_fk,uploaddate,songname,songfilepath) values(?,now(),?,?)";
            connection.query(query, [userid,fileName,username + "/" + fileName], (err, results) => {
                if (err) {
                    return res.status(500).json({ message: "Error while updating data------>" });
                }
                else {
                    res.status(200).json({
                        message: "Song Uploaded.",
                    })
                }
            });
        } else {
            res.status(400).json({
                message: "File Already Exist.",
            })
        }

    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: err,
        })
    }
});



//PAYLOAD    {path="username/songname.mp3"}
router.get('/getMusic/:username/:songname', function (req, res) {
    let data = req.body;
    let username = req.params.username;
    let songname = req.params.songname;
    const songPath = data.path;
    console.log(songPath);
    if (fs.existsSync(BaseURL + username+"/"+songname)) {
        ms.pipe(req, res, BaseURL + username+"/"+songname);
    } else {
        res.status(400).json({
            message: "Invalid Song Request.",
        })
    }
});


module.exports = router;