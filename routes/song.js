const express = require('express');
const connection = require('../connection');
const router = express.Router();
const path = require('path');
const util = require('util');
const { url } = require('inspector');

const app = express();

router.get('/getSubscribedSongs/:userid', (req, res, next) => {
    const userid = req.params.userid;
    console.log(userid);
    var query = "select u.username,us.songname,us.songfilepath,us.song_id from users u inner join usersongs us on u.id = us.user_fk where us.user_fk in (select subscribed_fk from subscribe where user_fk=?) order by uploaddate desc";
    connection.query(query, [userid], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        }
        else {
            return res.status(500).json(err);
        }
    });
});

router.get('/getMySongs/:userid', (req, res, next) => {
    const userid = req.params.userid;
    console.log(userid);
    var query = "select song_id,songname,songfilepath from usersongs where user_fk=?";
    connection.query(query, [userid], (err, results) => {
        if (!err) {
            return res.status(200).json(results);
        }
        else {
            return res.status(500).json(err);
        }
    });
});






module.exports = router;