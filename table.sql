CREATE TABLE product(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    description varchar(255),
    price integer,
    PRIMARY KEY(id)
);

create table users(
    id int not null AUTO_INCREMENT,
    uname varchar(255),
    contact varchar(255),
    email varchar(255),
    username varchar(255),
    pass varchar(255),
    songdesc varchar(501),
    PRIMARY KEY (id)
);

-- create table userSongs(
--     song_id int not null AUTO_INCREMENT,
--     user_fk int,
--     uploaddate date,
--     songfilepath varchar(500),
--     PRIMARY KEY (song_id),
--     FOREIGN KEY (user_fk) REFERENCES users(id)
-- )

create table userSongs(
    song_id int not null AUTO_INCREMENT,
    user_fk int,
    uploaddate datetime,
    songname varchar(500),
    songfilepath varchar(500),
    PRIMARY KEY (song_id)
);

create table subscribe(
    id int not null AUTO_INCREMENT,
    user_fk int,
    subscribed_fk int,
    PRIMARY KEY (id)
);