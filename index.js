const express = require('express');
var cors = require('cors');
const connection = require("./connection");
const productRoute = require('./routes/product');
const fileHandlingRoute = require('./routes/FileHandling');
const userRoute = require('./routes/user');
const songRoute = require('./routes/song')
const app = express();
const upload = require('express-fileupload')
const multer = require('multer')

// app.use(cors());
app.use(upload());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use('/product', productRoute);
app.use('/fileHandling', fileHandlingRoute);
app.use('/user', userRoute);
app.use('/song', songRoute);

module.exports = app;